<html>
<head>
<title>{$page_name}</title>
<link rel="stylesheet" type="text/css" href="style.css">
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
</head>

<body>
<div id="menu">
[<a href="?go=matches">Matches</a>]
[<a href="?go=players">Players</a>]
[<a href="?go=top">Top</a>]
[<a href="?go=maps">Maps</a>]
[<a href="?go=servers">Servers</a>]
[<a href="?go=links">Links</a>]
</div>

<div id="title">
<div id="titlebg">
    {if isset($levelshot) && $levelshot}
	   <div id="levelshot_container">
	    <div id="levelshot_label">{$match.map}</div>
        <div id="levelshot_image_container"><img src="levelshots/{$levelshot}" id='levelshot_image'></div>
	   </div>    
    {/if}
</div>
<div id="titletext">
{if $go=='matches'}
	[<a href="{$url}">All</a>]
	[<a href="{$url}&type=FFA">FFA</a>]
	[<a href="{$url}&type=TDM">TDM</a>]
	[<a href="{$url}&type=CTF">CTF</a>]
	<h1>{$page_name}, found {$matches_count}</h1>
{elseif $go=='match'}
	<h1>Match info</h1>
	<h2>{$match.type} on {$match.map} at {$match.time}</h2>
	Duration: {intval($match.duration / 60)} min {$match.duration - intval($match.duration / 60) * 60} sec
    <br /><g:plusone size="small" annotation="none"></g:plusone>
{elseif $go=='players'}
	[<a href="{$url}&type=humans">Humans</a>]
	[<a href="{$url}&type=bots">Bots</a>]
	<h1>Players list</h1>
{elseif $go=='player'}
	<h1>Player info</h1>
	<h2>{$player.name}{if $player.human!='t'} (bot){/if}</h2>
    <g:plusone size="small" annotation="none"></g:plusone>
{elseif $go=='top'}
	[<a href="{$url}">Previous week</a>]
	[<a href="{$url}&alltime">All time</a>]
	<h1>{$page_name}</h1>
{elseif $go=='maps'}
	<h1>{$page_name}</h1>
{elseif $go=='servers'}
	<h1>Servers</h1>
{elseif $go=='links'}
	<h1>Links</h1>
{/if}
</div>
</div>
<div id="body">

{if $go=='matches'}
   {foreach from=$matches item=match}
	  <a href="?go=match&match_id={$match.match_id}">{$match.time} - {$match.type} on {$match.map}</a><br />
   {/foreach}
{elseif $go=='match'}
    {if $match.team_game==1}
        Score: {if $match.red_name==''}Red{else}{$match.red_name}{/if} - {$match.red_score} : {if $match.blue_name==''}Blue{else}{$match.blue_name}{/if} - {$match.blue_score}<br><br>

        {if $match.red_name==''}Red{else}{$match.red_name}{/if} team:<br>
    	<table>
    	<tr>
    	<th>Name</th>
    	<th>Score</th>
    	<th>Kills</th>
        {if $match.type=='CTF'}
    	<th>Capt.</th>
    	<th>Ass.</th>
    	<th>Def.</th>
    	<th>Ret.</th>
        {/if}
        {if $match.type=='TDM'}
        <th>Net</th>
        {/if}
    	<th>Team dmg</th>
    	<th>Team kills</th>
    	<th>Deaths</th>
    	<th>Suicides</th>
    	<th>Hum.</th>
    	<th>Acc.</th>
    	</tr>
    	{foreach from=$players.red item=player}
    		<tr>
    		<td><a href="?go=player&player_id={$player.id}">{$player.name}</td>
    		<td>{$player.score}</td>
    		<td>{$player.kills}</td>
    		{if $match.type=='CTF'}
    		<td>{$player.captures}</td>
    		<td>{$player.assists}</td>
    		<td>{$player.defense}</td>
    		<td>{$player.returns}</td>
    		{/if}
    		{if $match.type=='TDM'}
    		<td>{$player.net}</td>
    		{/if}
    		<td>{$player.team_damage}</td>
    		<td>{$player.team_kills}</td>
    		<td>{$player.deaths}</td>
    		<td>{$player.suicides}</td>
    		<td>{$player.g_kills}</td>
    		<td>{$player.accuracy}%</td>
    		</tr>
    	{/foreach}
    	</table>

        <br>
        {if $match.blue_name==''}Blue{else}{$match.blue_name}{/if} team:<br>
    	<table>
    	<tr>
    	<th>Name</th>
    	<th>Score</th>
    	<th>Kills</th>
        {if $match.type=='CTF'}
    	<th>Capt.</th>
    	<th>Ass.</th>
    	<th>Def.</th>
    	<th>Ret.</th>
        {/if}
        {if $match.type=='TDM'}
        <th>Net</th>
        {/if}
    	<th>Team dmg</th>
    	<th>Team kills</th>
    	<th>Deaths</th>
    	<th>Suicides</th>
    	<th>Hum.</th>
    	<th>Acc.</th>
    	</tr>
    	{foreach from=$players.blue item=player}
    		<tr>
    		<td><a href="?go=player&player_id={$player.id}">{$player.name}</td>
    		<td>{$player.score}</td>
    		<td>{$player.kills}</td>
    		{if $match.type=='CTF'}
    		<td>{$player.captures}</td>
    		<td>{$player.assists}</td>
    		<td>{$player.defense}</td>
    		<td>{$player.returns}</td>
    		{/if}
    		{if $match.type=='TDM'}
    		<td>{$player.net}</td>
    		{/if}
    		<td>{$player.team_damage}</td>
    		<td>{$player.team_kills}</td>
    		<td>{$player.deaths}</td>
    		<td>{$player.suicides}</td>
    		<td>{$player.g_kills}</td>
    		<td>{$player.accuracy}%</td>
    		</tr>
    	{/foreach}
    	</table>
        
    {else}
    	<table>
    	<tr>
    	<th>Name</th>
    	<th>Score</th>
    	<th>Kills</th>
    	<th>Deaths</th>
    	<th>Suicides</th>
    	<th>Humiliations</th>
    	<th>Accuracy</th>
    	</tr>
    	{foreach from=$players item=player}
    		<tr>
    		<td><a href="?go=player&player_id={$player.id}">{$player.name}</td>
    		<td>{$player.score}</td>
    		<td>{$player.kills}</td>
    		<td>{$player.deaths}</td>
    		<td>{$player.suicides}</td>
    		<td>{$player.g_kills}</td>
    		<td>{$player.accuracy}%</td>
    		</tr>
    	{/foreach}
    	</table>
	{/if}
{elseif $go=='players'}
	{foreach from=$players item=player}
		<a href="?go=player&player_id={$player.id}">{$player.name}</a><br />
	{/foreach}
{elseif $go=='player'}
	{if count($aliases) > 0}
		{foreach from=$aliases item=alias}
			aka {$alias.name}
		{/foreach}
		<br>
		<br>
	{/if}
    <table>
    <tr><td>Matches</td><td>{$stats.total_matches}</td></tr>
	<tr><td>Wins</td><td>{$stats.wins}</td></tr>
	<tr><td>Efficiency</td><td>{$stats.efficiency}%</td></tr>
	<tr><td>Kills</td><td>{$stats.kills}</td></tr>
	<tr><td>Accuracy</td><td>{$stats.accuracy}%</td></tr>
	<tr><td>Humilations</td><td>{$stats.humilations}</td></tr>
	<tr><td>Deaths</td><td>{$stats.deaths}</td></tr>
	<tr><td>Suicides</td><td>{$stats.suicides}</td></tr>
	</table>
	<br />
	[<a href="?go=matches&player_id={$player.id}">{$player.name}'s matches</a>]
{elseif $go=='top'}
	<h2>Workaholics</h2>
	{if $workaholics}
	<table>
	<tr>
	<th>Name</th>
	<th>Matches</th>
	<th>Wins</th>
	<th>Efficiency</th>
	</tr>
	{foreach from=$workaholics item=workaholic}
		<tr>
		<td>{$workaholic.name}</td>
		<td>{$workaholic.total_matches}</td>
		<td>{$workaholic.wins}</td>
		<td>{$workaholic.efficiency}%</td>
		</tr>
	{/foreach}
	</table>
	{else}
	   None
	{/if}

	<h2>Serial killers</h2>
	{if $serial_killers}
	{foreach from=$serial_killers item=serial_killer}
		{$serial_killer.name} - {$serial_killer.total_kills} kills<br />
	{/foreach}
	{else}
	   None
	{/if}

	<h2>Maniacs</h2>
	{if $maniacs}
	{foreach from=$maniacs item=maniac}
		{$maniac.name} - {$maniac.gauntlet_kills} humilations<br />
	{/foreach}
	{else}
	   None
	{/if}

	<h2>Snipers</h2>
	{if $snipers}
	{foreach from=$snipers item=sniper}
		{$sniper.name} - accuracy {$sniper.accuracy}%<br />
	{/foreach}
	{else}
	   None
	{/if}

	<h2>Flag thiefs</h2>
	{if $flag_thiefs}
	{foreach from=$flag_thiefs item=flag_thief}
		{$flag_thief.name} - {$flag_thief.captures} captures<br />
	{/foreach}
	{else}
	   None
	{/if}

	<h2>Cannon fodder</h2>
	{if $cannon_fodders}
	{foreach from=$cannon_fodders item=cannon_fodder}
		{$cannon_fodder.name} - {$cannon_fodder.total_deaths} deaths<br />
	{/foreach}
	{else}
	   None
	{/if}

	<h2>Suicides</h2>
	{if $suicides}
	{foreach from=$suicides item=suicide}
		{$suicide.name} - {$suicide.total_suicides} suicides<br />
	{/foreach}
	{else}
	   None
	{/if}

	<h2>Team killers</h2>
	{if $team_killers}
	{foreach from=$team_killers item=team_killer}
		{$team_killer.name} - {$team_killer.teamkills} team kills<br />
	{/foreach}
	{else}
	   None
	{/if}
{elseif $go=='maps'}
    <div id="levelshots_container">
	{foreach from=$maps item=map}
	   <div id="levelshot_container">
	    <div id="levelshot_label">{$map.mapname}</div>
        <div id="levelshot_image_container"><img src="levelshots/{$map.screenshot}" id='levelshot_image'></div>
	   </div>    
	{/foreach}
	</div>
{elseif $go=='servers'}
	{foreach from=$servers item=srv}
		{if $srv.gq_online==1}
			<h2>{$srv.gq_hostname}</h2>
			Host: {$srv.gq_address} Port: {$srv.gq_port}<br>
			Game: {$srv.gamename} {$srv.version}<br>
			Mod: {$srv.game} {$srv.gameversion} {$srv.gamedate} <br>
			Gameplay: {$srv.server_gameplay}<br>
			Mode: {$srv.mode_current}<br>
			Map: {$srv.gq_mapname}<br> 
			Players: {$srv.gq_numplayers}/{$srv.gq_maxplayers}<br> 
			{if $srv.command_game==0}
				{if count($srv.active_players)>0}
					<br>
					<table>
					<tr>
					<th>Name</th>
					<th>Score</th>
					<th>Ping</th>
					</tr>
					{foreach from=$srv.active_players item=player}
						<tr>
						<td>{$player.nick}</td>
						<td>{$player.frags}</td>
						<td>{$player.ping}</td>
						</tr>
					{/foreach}
					</table>
				{/if}
				{if count($srv.spectators)>0}
					<br>
					Spectators:<br>					
					{foreach from=$srv.spectators item=spec}
						{$spec.nick}<br>
					{/foreach}
				{/if}
			{else}
				Score: Blue {$srv.Score_Blue} : Red {$srv.Score_Red}<br>
				{if count($srv.blue_players)>0}
					<br>
					Blue team:
					<table>
					<tr>
					<th>Name</th>
					<th>Score</th>
					<th>Ping</th>
					</tr>
					{foreach from=$srv.blue_players item=player}
						<tr>
						<td>{$player.nick}</td>
						<td>{$player.frags}</td>
						<td>{$player.ping}</td>
						</tr>
					{/foreach}
					</table>
				{/if}

				{if count($srv.red_players)>0}
					<br>
					Red team:
					<table>
					<tr>
					<th>Name</th>
					<th>Score</th>
					<th>Ping</th>
					</tr>
					{foreach from=$srv.red_players item=player}
						<tr>
						<td>{$player.nick}</td>
						<td>{$player.frags}</td>
						<td>{$player.ping}</td>
						</tr>
					{/foreach}
					</table>
				{/if}
				
				{if count($srv.spectators)>0}
					<br>
					Spectators:<br>
					{foreach from=$srv.spectators item=spec}
						{$spec.nick}<br>
					{/foreach}
				{/if}
			{/if}
		{/if}
	{/foreach}
{elseif $go=='links'}
	<a href="../pk3">Quake 3 Distributive and maps</a>
{/if}

{if isset($paginator) and count($paginator) > 1}
	<br />
	<center>
	[<a href="{$url}&page=1">&lt;&lt;&lt;</a>]
	{foreach from=$paginator item=page}
		[<a href="{$url}&page={$page.pageNo}">{$page.pageNo}</a>]
	{/foreach}
	[<a href="{$url}&page={$last_page}">&gt;&gt;&gt;</a>]
	</center>
{/if}
</div>
<center>{$pagegen_time}</center>
</body>
</html>
