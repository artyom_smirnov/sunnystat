<?php
define('MATCHES_ON_PAGE', 30);
define('ROOT', '/home/artem/public_html/sunnystat');
define('LEVELSHOTS', ROOT.'/levelshots');

$mtime = microtime(); 
$mtime = explode(" ",$mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$starttime = $mtime; 

$servers = array(
    'FFA' => array('quake3', '10.81.1.77', 27960),
    'CTF' => array('quake3', '10.81.1.77', 27961),
    'TDM' => array('quake3', '10.81.1.77', 27962),
    'IFFA' => array('quake3', '10.81.1.77', 27963),
   // 'NOLAN' => array('quake3', 'q3.nolan.net.ua', 27960),
   // 's1' => array('quake3', '195.245.97.3')
);

require(ROOT.'/smarty/Smarty.class.php');
require(ROOT.'/gameq/GameQ.php');

$smarty = new Smarty();
$smarty->setTemplateDir(ROOT.'/_smarty/templates');
$smarty->setCompileDir(ROOT.'/_smarty/templates_c');
$smarty->setCacheDir(ROOT.'/_smarty/cache');
$smarty->setConfigDir(ROOT.'/_smarty/configs');

$go = isset($_GET['go']) ? $_GET['go'] : 'matches';

$dbconn = pg_connect('host=localhost dbname=sunnystat user=sunnystat password=sunnystat');

$selectedTab = array('players' => '', 'matches' => '');

$url="?go=".$go;

function compare_frags($a, $b)
{
    return $a['frags'] < $b['frags'];
}

switch($go)
{
	case 'matches':
	default:
		$go = 'matches';

		$selectedTab['matches'] = 'selected';
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		
		if (!isset($_GET['player_id']))				
		{
			$page_name = 'Matches';

			$filter = '';
			$filter_arr = array();
			if (isset($_GET['type']))
			{
				$filter = ' where type = $1 ';
				array_push($filter_arr, $_GET['type']);
				$page_name .= " (" .$_GET['type']. ")";
				$url .= '&type='.$_GET['type'];
			}

			$smarty->assign('page_name', $page_name);	
		
			$sql = 'select count(*) from match_with_humans ';
			$sql .= $filter; 
			$res = pg_prepare($dbconn, 'paginator', $sql);
			$data = pg_fetch_all(pg_execute($dbconn, 'paginator', $filter_arr));
			$matches_count = $data[0]['count'];
			$pages = paginator($matches_count, MATCHES_ON_PAGE, $page);

			array_push($filter_arr, MATCHES_ON_PAGE);
			array_push($filter_arr, ($page - 1) * MATCHES_ON_PAGE);
			$sql = '
				select *, id match_id from match_with_humans ';
			$sql .= $filter;
			$sql .= ' order by time desc limit ';
			$sql .= '$' . strval(count($filter_arr) - 1) . ' offset $' . strval(count($filter_arr));

			$res = pg_prepare($dbconn, 'matches', $sql);	
			$data = pg_execute($dbconn, 'matches', $filter_arr);
			$matches = pg_fetch_all($data);
		}
		else
		{
			$sql = 'select * from player where id=$1';
			$res = pg_prepare($dbconn, 'player', $sql);
			$data = pg_fetch_all(pg_execute($dbconn, 'player', array($_GET['player_id'])));
			if (!$data)
				die('No such player');
			$page_name = $data[0]['name'] . "'s matches";
	
			$filter = '';
			$filter_arr = array($_GET['player_id']);

			if (isset($_GET['type']))
			{
				$filter = ' and type = $2 ';
				array_push($filter_arr, $_GET['type']);
				$page_name .= ' (' . $_GET['type'] . ')';
				$url .= '&type='.$_GET['type'];
			}
			
			$sql = 'select count(*) from match_player mp join match m on mp.match_id = m.id where player_id = $1 ';
			$sql .= $filter;
			$res = pg_prepare($dbconn, 'paginator', $sql);
			$data = pg_fetch_all(pg_execute($dbconn, 'paginator', $filter_arr));
			$matches_count = $data[0]['count'];
			$pages = paginator($matches_count, MATCHES_ON_PAGE, $page);			
			
			$sql = 'select * from match_player mp join match m on mp.match_id = m.id where player_id = $1 '; 
			$sql .= $filter;
			$sql .= ' order by time desc ';
			
			if ($matches_count > 0)
			{
				array_push($filter_arr, MATCHES_ON_PAGE);
				array_push($filter_arr, ($page - 1) * MATCHES_ON_PAGE);
				$sql .= ' limit $' . strval(count($filter_arr) - 1) . ' offset $' . strval(count($filter_arr));

				$res = pg_prepare($dbconn, 'matches', $sql);	
				$data = pg_execute($dbconn, 'matches', $filter_arr);
				$matches = pg_fetch_all($data);
			}
			else
			{
				$matches = array();
			}

			$smarty->assign('page_name', $page_name);
			$url .= '&player_id='.$_GET['player_id'];
		}
		
		$smarty->assign('matches_count', $matches_count);
		$smarty->assign('last_page', intval($matches_count / MATCHES_ON_PAGE) + 1);

		$smarty->assign('matches', $matches);
		$smarty->assign('paginator', $pages);
		break;

	case 'match':
		$smarty->assign('page_name', 'Match');
		
		if (!isset($_GET['match_id']))
			die('Need match id');		
		$match_id = $_GET['match_id'];

		$res = pg_prepare($dbconn, 'match', 'select * from match where id=$1');
		$data = pg_execute($dbconn, 'match', array($match_id));
		$match = pg_fetch_all($data);
		
		if ($match[0]['type'] == 'FFA')
		{
			$res = pg_prepare($dbconn, 'players', '
				select mp.player_id id, p.name,
                score, kills, deaths, suicides, g_kills,
				accuracy(
	  				bfg_shots, bfg_hits,
	  				g_shots, g_hits,
	  				gl_shots, gl_hits,
	  				lg_shots, lg_hits,
	  				mg_shots, mg_hits,
	 				pg_shots, pg_hits,
	  				rg_shots, rg_hits,
	  				rl_shots, rl_hits,
	  				sg_shots, sg_hits)
				accuracy
				from match_player mp
				join player p on mp.original_player_id = p.id
				where match_id=$1
				order by score desc');
		
			$data = pg_execute($dbconn, 'players', array($match_id));
			$players = pg_fetch_all($data);
			$match[0]['team_game'] = 0;
		}
		else
		{
			$res = pg_prepare($dbconn, 'players', '
				select mp.player_id id, p.name,
                score, kills, deaths, suicides, g_kills,
                team_damage, team_kills,
                captures, assists, defense, returns,
				accuracy(
	  				bfg_shots, bfg_hits,
	  				g_shots, g_hits,
	  				gl_shots, gl_hits,
	  				lg_shots, lg_hits,
	  				mg_shots, mg_hits,
	 				pg_shots, pg_hits,
	  				rg_shots, rg_hits,
	  				rl_shots, rl_hits,
	  				sg_shots, sg_hits)
				accuracy,
				(kills - deaths) net
				from match_player mp
				join player p on mp.original_player_id = p.id
				where match_id=$1 and team=$2
				order by score desc');
		
			$data = pg_execute($dbconn, 'players', array($match_id, 1));
			$red_players = pg_fetch_all($data);

			$data = pg_execute($dbconn, 'players', array($match_id, 2));
			$blue_players = pg_fetch_all($data);
			
			$players['red'] = $red_players;
			$players['blue'] = $blue_players;
			$match[0]['team_game'] = 1;
		}
		
		#levelshot image
		$levelshot_file=$match[0]['map'].'.jpg';
		if (!file_exists(LEVELSHOTS.'/'.$levelshot_file))
		{
            $levelshot_file = NULL;
		}
		
		$smarty->assign('match', $match[0]);
		$smarty->assign('players', $players);
		$smarty->assign('levelshot', $levelshot_file);

		break;

	case 'players':
		$smarty->assign('page_name', 'Players');
		$selectedTab['players'] = 'selected';

        if (isset($_GET['type']) && $_GET['type'] == 'bots')
        {
		  $res = pg_query($dbconn, 'select * from player where alias_id is null and not human order by name');
        }
        else
        {
		  $res = pg_query($dbconn, 'select * from player where alias_id is null and human order by name');
        }
		$players = pg_fetch_all($res);

		$smarty->assign('players', $players);
		break;

	case 'player':
		$smarty->assign('page_name', 'Player');
		$selectedTab['players'] = 'selected';
		
		if (!isset($_GET['player_id']))
		{
			die('Need player id');		
		}
		
		$player_id = $_GET['player_id'];

		$res = pg_prepare($dbconn, 'player', 'select * from player where id = $1');
		$player_data = pg_fetch_all(pg_execute($dbconn, 'player', array($player_id)));
		
		if ($player_data[0]['alias_id'])
		{
			die('This is alias');
		}
		
		$res = pg_prepare($dbconn, 'aliases', 'select * from player where alias_id = $1');
		$aliases_data = pg_fetch_all(pg_execute($dbconn, 'aliases', array($player_data[0]['id'])));

		if (!$aliases_data)
			$aliases_data = array();
		
		$res = pg_prepare($dbconn, 'stats', '
			select
			player_wins(p.id) wins,
			cast((cast(player_wins(p.id) as float)/count(*) * 100) as integer) efficiency,
		    p.name,
		    count(*) total_matches,
		    sum(kills) kills,
		    sum(suicides) suicides,
		    sum(g_kills) humilations,
		    sum(deaths) deaths,
		    sum(accuracy(
		      bfg_shots, bfg_hits,
		      g_shots, g_hits,
		      gl_shots, gl_hits,
		      lg_shots, lg_hits,
		      mg_shots, mg_hits,
		      pg_shots, pg_hits,
		      rg_shots, rg_hits,
		      rl_shots, rl_hits,
		      sg_shots, sg_hits))/count(*) accuracy
		    from match_player mp
		  join player p on mp.player_id = p.id
		  where mp.player_id = $1
		  group by p.id

		  ');
		$stats = pg_fetch_all(pg_execute($dbconn, 'stats', array($player_id)));

/*
		$res = pg_prepare($dbconn, 'favweapon',
			"select weapon, shots
				from
				(
					select sum(bfg_shots) shots, 'BFG' weapon from match_player
						union select sum(g_hits), 'Gauntlet' weapon from match_player
						union select sum(gl_shots), 'Grenade launcher' weapon from match_player
						union select sum(lg_shots), 'Lightning gun' weapon from match_player
						--union select sum(mg_shots), 'Machinegun' weapon from match_player
						union select sum(pg_shots), 'Plasma gun' weapon from match_player
						union select sum(rg_shots), 'Railgun' weapon from match_player
						union select sum(rl_shots), 'Rocket launcher' weapon from match_player
						union select sum(sg_shots), 'Shotgun' weapon from match_player
					where player_id = $1
				) wshots
				order by shots desc
				limit 1");
		$favweapon = pg_fetch_all(pg_execute($dbconn, 'favweapon', array($player_id)));
*/

		$smarty->assign('player', $player_data[0]);
		$smarty->assign('aliases', $aliases_data);
		$smarty->assign('stats', $stats[0]);
		//$smarty->assign('favweapon', $favweapon[0]);			
		break;

	case 'top':
		$smarty->assign('page_name', 'Top');
		
        if (isset($_GET['alltime']))
        {
            $spanCondition1 = '';
            $spanCondition2 = '';
            $smarty->assign('page_name', 'All time top');
		}
		else
		{
            $spanCondition1 = ' and date_trunc(\'day\', time) < date_trunc(\'week\', now()) and date_trunc(\'day\', time) >= date_trunc(\'week\', now()) - interval \'1 week\'';
            $spanCondition2 = ' join match on match.id = match_id where date_trunc(\'day\', time) < date_trunc(\'week\', now()) and date_trunc(\'day\', time) >= date_trunc(\'week\', now()) - interval \'1 week\'';
            $smarty->assign('page_name', 'Previous week top');
		}

		$res = pg_query($dbconn, '
            select p.name, w.wins, g.total_matches, cast(cast(w.wins as float) / g.total_matches * 100 as integer) efficiency from
            --wins
            (
                select player_id, count(player_id) wins from
                (select match_id, player_id, rank() over (partition by match_id order by score desc) as place from match_player mp
                    '.$spanCondition2.'
                ) places
                where place = 1
                group by player_id
            ) w
            join
            --total games
            (
                select player_id, count(player_id) total_matches from match_player
                '.$spanCondition2.'
                group by player_id
            ) g
            on w.player_id = g.player_id
            join player p on w.player_id = p.id
            where p.human
            order by wins desc
            limit 10
        ');

		$workaholics = pg_fetch_all($res);

		$res = pg_query($dbconn, '
			select p.name, sum(kills) total_kills from match_player mp
			join player p on mp.player_id = p.id
			join match on match.id = match_id 
			where human and kills > 0 '.$spanCondition1.'
			group by p.id
			order by total_kills desc
			limit 10');
		$serial_killers = pg_fetch_all($res);

		$res = pg_query($dbconn, '
			select p.name, sum(g_kills) gauntlet_kills from match_player mp
			join player p on mp.player_id = p.id
			join match on match.id = match_id 
			where human and g_kills > 0 '.$spanCondition1.'
			group by p.id
			order by gauntlet_kills desc
			limit 10');
		$maniacs = pg_fetch_all($res);

		$res = pg_query($dbconn, '
			select p.name,
			sum(accuracy(
  				bfg_shots, bfg_hits,
  				g_shots, g_hits,
  				gl_shots, gl_hits,
  				lg_shots, lg_hits,
  				mg_shots, mg_hits,
 				pg_shots, pg_hits,
  				rg_shots, rg_hits,
  				rl_shots, rl_hits,
  				sg_shots, sg_hits))/count(*)
  			accuracy
			from match_player mp
			join player p on mp.player_id = p.id
			join match on match.id = match_id 
			where human '.$spanCondition1.'
			group by p.id
			order by accuracy desc
			limit 10');
		$snipers = pg_fetch_all($res);

		$res = pg_query($dbconn, '
			select p.name, sum(suicides) total_suicides from match_player mp
			join player p on mp.player_id = p.id
			join match on match.id = match_id 
			where human and suicides > 0 '.$spanCondition1.'
			group by p.id
			order by total_suicides desc
			limit 10');
		$suicides = pg_fetch_all($res);
		
		$res = pg_query($dbconn, '
			select p.name, sum(deaths) total_deaths from match_player mp
			join player p on mp.player_id = p.id
			join match on match.id = match_id 
			where human and deaths > 0 '.$spanCondition1.'
			group by p.id
			order by total_deaths desc
			limit 10');
		$cannon_fodders = pg_fetch_all($res);

		$res = pg_query($dbconn, '
			select p.name, sum(team_kills) teamkills from match_player mp
			join player p on mp.player_id = p.id
			join match on match.id = match_id 
			where human and team_kills > 0 '.$spanCondition1.'
			group by p.id
			order by teamkills desc
			limit 10');
		$team_killers = pg_fetch_all($res);

		$res = pg_query($dbconn, '
			select p.name, sum(captures) captures from match_player mp
			join player p on mp.player_id = p.id
			join match on match.id = match_id 
			where human and captures > 0 '.$spanCondition1.'
			group by p.id
			order by captures desc
			limit 10');
		$flag_thiefs = pg_fetch_all($res);
		
		$smarty->assign('workaholics', $workaholics);
		$smarty->assign('serial_killers', $serial_killers);
		$smarty->assign('maniacs', $maniacs);
		$smarty->assign('snipers', $snipers);
		$smarty->assign('flag_thiefs', $flag_thiefs);
		$smarty->assign('cannon_fodders', $cannon_fodders);
		$smarty->assign('suicides', $suicides);
		$smarty->assign('team_killers', $team_killers);
		
		break;
    case 'maps':
		$smarty->assign('page_name', 'Maps popularity');
		$res = pg_query($dbconn, 'select map mapname, count(map) matches from match_with_humans group by map order by matches desc');
		$maps = pg_fetch_all($res);
		
		foreach ($maps as &$map)
		{
            $levelshot_file=$map['mapname'].'.jpg';
    		if (!file_exists(LEVELSHOTS.'/'.$levelshot_file))
    		{
                $levelshot_file = NULL;
    		}
    		$map['screenshot'] = $levelshot_file;
		}

		$smarty->assign('maps', $maps);

        break;

	case 'servers':
		$smarty->assign('page_name', 'Servers');
		$gq = new GameQ();
		$gq->addServers($servers);
		$gq->setOption('timeout', 500);
		$gq->setFilter('normalise');
		$gq->setFilter('stripcolor');

		$servers_data = $gq->requestData();

		foreach($servers_data as &$srv)
		{
			if (array_key_exists('Players_Active', $srv))
			{
                //Sort only for FFA matches to get proper teams
				$gq->setFilter('sortplayers', 'gq_score');
				$srv['command_game'] = 0;
				$players_nums = explode(' ', trim($srv['Players_Active']));
				$active_players = array();
				$spectators = array();
				foreach($srv['players'] as $i => $player)
				{
					if (in_array($i + 1, $players_nums))
					{
						array_push($active_players, $player);
					}
					else
					{
						array_push($spectators, $player);
					}
				}
				$srv['spectators'] = $spectators;
				$srv['active_players'] = $active_players;
			}
			else if (array_key_exists('Players_Blue', $srv))
			{
				$srv['command_game'] = 1;
				$blue = explode(' ', trim($srv['Players_Blue']));
				$red = explode(' ', trim($srv['Players_Red']));
				
				$blue_players = array();
				$red_players = array();
				$spectators = array();
				foreach($srv['players'] as $i => $player)
				{
					if (in_array($i + 1, $blue))
					{
						array_push($blue_players, $player);
					}
					else if (in_array($i + 1, $red))
					{					
						array_push($red_players, $player);
					}
					else
					{
						array_push($spectators, $player);
					}
				}
				
                //Sort by frags manually
                usort($blue_players, 'compare_frags');
                usort($red_players, 'compare_frags');
				
				$srv['spectators'] = $spectators;
				$srv['blue_players'] = $blue_players;
				$srv['red_players'] = $red_players;
			}
			else
			{
				$srv['command_game'] = 0;
				$srv['active_players'] = array();
				$srv['spectators'] = $srv['players'];
			}			
		}

        //print_r($servers_data['FFA']);

		$smarty->assign('servers', $servers_data);
		break;
	case 'links':
		$smarty->assign('page_name', 'Links');
		break;
}

$mtime = microtime(); 
$mtime = explode(" ",$mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$endtime = $mtime; 
$totaltime = ($endtime - $starttime); 

$smarty->assign('selectedTab', $selectedTab);
$smarty->assign('go', $go);
$smarty->assign('url', $url);
$smarty->assign('pagegen_time', $totaltime);


$smarty->display('index.tpl');

pg_close($dbconn);

function paginator($total, $limit, $current)
{
    $pages = array();
    $maxPages = intval(($total / $limit)) + 1;
    $maxTabs = 20;
    $page = $current - $maxTabs / 2;
    if ($page <= 0)
        $page = 1;
    if ($maxPages > $maxTabs)
    {
        if ($maxPages - $page < $maxTabs)
        {
            $page = $page - ($maxTabs - ($maxPages - $page)) + 1;
        }
    }

    while ($maxTabs > 0 && $page <= $maxPages)
    {
        $pages[] = array(
            'pageNo' => $page,
    			//'offset' => $countedPages
        );
        $page++;
        $maxTabs--;
    }
    
	return $pages;
}

?>
