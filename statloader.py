#!/usr/bin/python

import psycopg2
import sys
import xml.sax
import exceptions

class Error(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)

class Qa3XmlStatsHandler(xml.sax.ContentHandler):
    def __init__(self, conn):
        self.conn = conn
        self.stats = {'TeamDamage':0,'TeamKills':0,'Captures':0, 'Assists':0, 'Defense':0, 'Returns':0}
        self.weapons = {'BFG':(0,0,0), 'G':(0,0,0), 'GL':(0,0,0), 'LG':(0,0,0),
            'MG':(0,0,0), 'PG':(0,0,0), 'RG':(0,0,0), 'RL':(0,0,0), 'SG':(0,0,0)}
        self.items = {'MH':(0), 'RA':(0), 'GA':(0), 'YA':(0),
            'BattleSuit':(0,0), 'Flight':(0,0), 'Haste':(0,0), 'Invis':(0,0),
            'Quad':(0,0), 'Regen':(0,0), 'Blue Flag':(0,0), 'Ref Flag':(0,0)}
        self.cur = conn.cursor()
        self.team = None
        self.team_score = 0
        self.red_score = 0
        self.blue_score = 0
        self.red_name = ''
        self.blue_name = ''

    def startElement(self, name, attrs):
        if name == 'match':
            if attrs.get('type') not in ('FFA', 'TDM', 'CTF'):
                raise Error('Game ' + attrs.get('type') + 'is not supported')
            result = self.cur.execute("insert into match(time, map, duration, type) values(%s, %s, %s, %s) returning id", 
                (attrs.get('datetime'), attrs.get('map'), int(attrs.get('duration')), attrs.get('type')) )
            self.match_id = self.cur.fetchone()[0]
        elif name == 'player':
            self.current_player_name = attrs.get('name')
        elif name == 'stat':
            self.stats[attrs.get('name')] = int(attrs.get('value'))
        elif name == 'weapon':
            self.weapons[attrs.get('name')] = \
                (int(attrs.get('shots')), int(attrs.get('hits')), int(attrs.get('kills')))
        elif name == 'item':
            if attrs.has_key('time'):
                self.items[attrs.get('name')] = \
                    (int(attrs.get('pickups')), int(attrs.get('time')))
            else:
                self.items[attrs.get('name')] = \
                    (int(attrs.get('pickups')), )
        elif name == 'team':
            self.team = 1 if self.team == None else 2
            self.team_score = attrs.get('score')
            if self.team == 1:
                self.red_score = self.team_score
                self.red_name = attrs.get('name')
            else:
                self.blue_score = self.team_score
                self.blue_name = attrs.get('name')

    def endElement(self, name):
        if name == 'match':
            result = self.cur.execute("update match set red_score=%s, blue_score=%s, red_name=%s, blue_name=%s where id=%s", 
                (self.red_score, self.blue_score, self.red_name, self.blue_name, self.match_id))
            self.conn.commit()
        if name == 'player':
            w = self.weapons
            i = self.items
            
            self.cur.execute("select id, alias_id from player where name = %s", (self.current_player_name,))
            res = self.cur.fetchone()
            if res:
                player_id = res[1] if res[1] else res[0]
                original_player_id = res[0]
            else:
                self.cur.execute("insert into player(name) values(%s) returning player.id as id", (self.current_player_name,))
                res = self.cur.fetchone()
                player_id = res[0]
                original_player_id = res[0]

            self.cur.execute("insert into match_player(match_id, player_id, original_player_id,\
                armor_total, damage_given, damage_taken,\
                deaths, health_total, kills, net, score, suicides,\
                bfg_shots, bfg_hits, bfg_kills,\
                g_shots, g_hits, g_kills,\
                gl_shots, gl_hits, gl_kills,\
                lg_shots, lg_hits, lg_kills,\
                mg_shots, mg_hits, mg_kills,\
                pg_shots, pg_hits, pg_kills,\
                rg_shots, rg_hits, rg_kills,\
                rl_shots, rl_hits, rl_kills,\
                sg_shots, sg_hits, sg_kills,\
                green_armor, mega_health, red_armor, yellow_armor,\
                battle_suit_p, battle_suit_t,\
                flight_p, flight_t,\
                regenerate_p, regenerate_t,\
                haste_p, haste_t,\
                invis_p, invis_t,\
                quad_p, quad_t,\
                team,\
                team_damage, team_kills,\
                captures, assists, defense, returns)\
                values(%s, %s, %s,\
                %s,\
                %s, %s,\
                %s, %s,\
                %s, %s,    %s,\
                %s,\
                %s, %s, %s,\
                %s, %s, %s,\
                %s, %s, %s,\
                %s, %s, %s,\
                %s, %s, %s,\
                %s, %s, %s,\
                %s, %s, %s,\
                %s, %s, %s,\
                %s, %s, %s,\
                %s, %s, %s, %s,\
                %s, %s,\
                %s, %s,\
                %s, %s,\
                %s, %s,\
                %s, %s,\
                %s, %s,\
                %s,\
                %s, %s,\
                %s, %s, %s, %s)",
                (self.match_id, player_id, original_player_id,
                self.stats['ArmorTotal'],\
                self.stats['DamageGiven'], self.stats['DamageTaken'],\
                self.stats['Deaths'], self.stats['HealthTotal'],\
                self.stats['Kills'], self.stats['Net'], self.stats['Score'],\
                self.stats['Suicides'],
                w['BFG'][0], w['BFG'][1], w['BFG'][2],
                w['G'][0], w['G'][1], w['G'][2],
                w['GL'][0], w['GL'][1], w['GL'][2],
                w['LG'][0], w['LG'][1], w['LG'][2],
                w['MG'][0], w['MG'][1], w['MG'][2],
                w['PG'][0], w['PG'][1], w['PG'][2],
                w['RG'][0], w['RG'][1], w['RG'][2],
                w['RL'][0], w['RL'][1], w['RL'][2],
                w['SG'][0], w['SG'][1], w['SG'][2],
                i['GA'][0], i['MH'][0], i['RA'][0], i['YA'][0],
                i['BattleSuit'][0], i['BattleSuit'][1], 
                i['Flight'][0], i['Flight'][1], 
                i['Regen'][0], i['Regen'][1], 
                i['Haste'][0], i['Haste'][1], 
                i['Invis'][0], i['Invis'][1], 
                i['Quad'][0], i['Quad'][1],
                self.team,
                self.stats['TeamDamage'], self.stats['TeamKills'],
                self.stats['Captures'], self.stats['Assists'], self.stats['Defense'], self.stats['Returns']))

def main(argv):
    conn = psycopg2.connect("host=localhost dbname=sunnystat user=sunnystat password=sunnystat")

    #conn.query("delete from match")
    parser = xml.sax.make_parser()
    
    for f in argv:
        parser.setContentHandler(Qa3XmlStatsHandler(conn))
        try:
            print("Loading '%s'" % f)
            parser.parse(open(f, "r"))
            print("OK")
        except KeyboardInterrupt:
            print("Exit on keyboard interrupt")
            exit()
        except psycopg2.Error, e:
            print(e.pgerror)
        except exceptions.TypeError, e:
            print(e)
        except Error, e:
            print("Error: %s" % e.msg)
        except:
            print("Unexpected error: %s" % sys.exc_info()[0])

    conn.close()
    
if __name__ == '__main__':
    main(sys.argv[1:])
