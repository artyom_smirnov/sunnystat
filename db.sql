drop table if exists player cascade;
drop table if exists match cascade;
drop table if exists match_player cascade;

drop sequence if exists match_id_seq cascade;
drop sequence if exists match_player_id_seq cascade;
drop sequence if exists player_id_seq cascade;

create sequence match_id_seq;
create sequence match_player_id_seq;
create sequence player_id_seq;

create table player(
	id bigint primary key default nextval('player_id_seq'),
	alias_id bigint references player(id),
	name varchar unique,
	human boolean default false);

create table match
(
	id bigint primary key default nextval('match_id_seq'),
	time timestamp,
	map varchar,
	duration int,
	type varchar,
	red_score int,
	blue_score int,
	red_name varchar,
	blue_name varchar
);

create table match_player
(
	id bigint primary key default nextval('match_player_id_seq'),
	match_id bigint references match (id) on delete cascade,
	player_id bigint references player (id) on delete cascade,
	original_player_id bigint references player (id) on delete cascade,
	armor_total int,
	damage_given int,
	damage_taken int,
	deaths int,
	health_total int,
	kills int,
	net int,
	score int,
	suicides int,
	
	--Weapons
	--BFG(BFG)
	bfg_shots int,
	bfg_hits int,
	bfg_kills int,

	--Gauntlet(G)
	g_shots int,
	g_hits int,
	g_kills int,

	--Grenade launcher(GL)
	gl_shots int,
	gl_hits int,
	gl_kills int,
	
	--Lighning gun(LG)
	lg_shots int,
	lg_hits int,
	lg_kills int,

	--Machinegun(MG)
	mg_shots int,
	mg_hits int,
	mg_kills int,

	--Plasma gun(PG)
	pg_shots int,
	pg_hits int,
	pg_kills int,

	--Railgun(RG)
	rg_shots int,
	rg_hits int,
	rg_kills int,

	--Rocket launcher(RL)
	rl_shots int,
	rl_hits int,
	rl_kills int,

	--Shotgun(SG)
	sg_shots int,
	sg_hits int,
	sg_kills int,
	
	--Items
	green_armor int, --Green armor(GA)
	mega_health int, --Mega health(MH)
	red_armor int, --Red armor(RA)
	yellow_armor int, --Yellow armor(YA)

	--Powerups
	battle_suit_p int, --Battle suit(BattleSuit)
	battle_suit_t int,
	flight_p int,	--Flight(Flight)
	flight_t int,
	regenerate_p int, --Regenerate(Regen) 
	regenerate_t int,
	haste_p int, --Haste(Haste)
	haste_t int,
	invis_p int, 	--Invisibility(Invis)
	invis_t int,
	quad_p int, --Quad damage(Quad)
	quad_t int,
	
	team int, --red=1 blue=2
	team_damage int,
	team_kills int,
	
	captures int,
	assists int,
	defense int,
	returns int
);

create or replace function player_id_by_name(someName varchar) returns bigint as
$body$
declare id bigint;
begin
  select into id p.id from player p where name = $1;
  if id is not null then
    return id;
  end if;

  select into id p2.id from player p1 join player p2 on p1.id = p2.alias_id where p2.name = $1;
  if id is not null then
    return id;
  end if;

  insert into player(name) values($1) returning player.id into id;
  return id;
end;
$body$
language plpgsql;

create or replace function set_alias(playerId bigint, aliasId bigint) returns void as
$body$
begin
  update player set alias_id = $1 where id = $2;
  update match_player set player_id = $1 where original_player_id = $2;
end;
$body$
language plpgsql;

create or replace function undo_alias(aliasId bigint) returns void as
$body$
begin
  update player set alias_id = NULL where id = $1;
  update match_player set player_id = $1 where original_player_id = $1;
end;
$body$
language plpgsql;

create or replace view match_with_humans as
select * from match where id in (
    select distinct match_id
    from match_player mp
    join player p on mp.player_id = p.id
    where human = true);

create or replace function accuracy(
  bfg_shots integer, bfg_hits integer,
  g_shots integer, g_hits integer,
  gl_shots integer, gl_hits integer,
  lg_shots integer, lg_hits integer,
  mg_shots integer, mg_hits integer,
  pg_shots integer, pg_hits integer,
  rg_shots integer, rg_hits integer,
  rl_shots integer, rl_hits integer,
  sg_shots integer, sg_hits integer
)
returns integer as
$body$
declare sum float;
declare count integer;
begin
  count = 0;
  sum = 0;

  if bfg_shots != 0 then count = count + 1; sum = sum + (cast(bfg_hits as float)/bfg_shots); end if;
  if g_shots != 0 then count = count + 1; sum = sum + (cast(g_hits as float)/g_shots); end if;
  if gl_shots != 0 then count = count + 1; sum = sum + (cast(gl_hits as float)/gl_shots); end if;
  if lg_shots != 0 then count = count + 1; sum = sum + (cast(lg_hits as float)/lg_shots); end if;
  if mg_shots != 0 then count = count + 1; sum = sum + (cast(mg_hits as float)/mg_shots); end if;
  if pg_shots != 0 then count = count + 1; sum = sum + (cast(pg_hits as float)/pg_shots); end if;
  if rg_shots != 0 then count = count + 1; sum = sum + (cast(rg_hits as float)/rg_shots); end if;
  if rl_shots != 0 then count = count + 1; sum = sum + (cast(rl_hits as float)/rl_shots); end if;
  if sg_shots != 0 then count = count + 1; sum = sum + (cast(sg_hits as float)/sg_shots); end if;

  if count = 0 then
    return 0;
  else
    return cast((sum / count) * 100 as integer);
  end if;
end;
$body$
language plpgsql;

create or replace function player_wins(pid bigint) returns bigint as
$body$
declare wins int;
begin
  select into wins count(*)
    from (
      select player_id, match_id, score,
        max(score) over (partition by match_id) max_score
        from match_player
    ) m
    where score = max_score and player_id = $1
    group by player_id;
    
  if not found then
    return 0;
  else
    return wins;
  end if;
end;
$body$
language plpgsql;

insert into player(name, human) values
	('urandom', true),   		--1
	('scientist', true), 		--2
	('I''m the flyer', true),   --3
	('Begemot', true),   		--4
	('[Dr]Wolf', true),  		--5
	('macel', true),     		--6
	('pessimist', true), 		--7
	('wlad', true),      		--8
	('pdv', true),       		--9
	('Gureev A.P.', true),       --10
	('First!', true),       --11
	('destr', true),       --12
	('XaBbl4', true),       --13
	('UnnamedPlayer', true), --14
	('VITALY', true), --15
	('Ebanarium', true), --16
	('-=Lightfore=-', true), --17
	('qqq', true), --18
	('Stasyan', true) --19
	;

insert into player(alias_id, name, human) values
	(2, 'ershov pidar', true),
	(3, 'Chingizhan', true),
	(3, 'Andrushenko', true),
	(3, 'Andrianoff', true),
	(3, 'Sysanin', true),
	(3, 'Rustam', true),
	(5, 'DrWolf', true),
	(6, 'milashka', true),
	(6, 'Milashka', true),
	(6, 'milashaka', true),
	(9, 'PDV', true),
	(10, 'podstakannik', true),
	(11, 'First', true),
	(12, 'dstr', true),
	(13, 'XaBbl', true)
	;

commit;
